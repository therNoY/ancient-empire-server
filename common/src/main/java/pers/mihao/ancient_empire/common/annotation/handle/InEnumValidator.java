package pers.mihao.ancient_empire.common.annotation.handle;

import pers.mihao.ancient_empire.common.annotation.validated.InEnum;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * 验证字符串是在枚举中
 * @version 1.0
 * @author mihao
 * @date 2019\8\26 0001 22:21
 */
public class InEnumValidator implements ConstraintValidator<InEnum, Object> {

    Class<? extends Enum> anEnum;

    String message;

    @Override
    public void initialize(InEnum constraintAnnotation) {
        anEnum = constraintAnnotation.in();
        message = constraintAnnotation.message();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        if (value == null) {
            return false;
        }
        return true;
    }

}
