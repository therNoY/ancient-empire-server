package pers.mihao.ancient_empire.common.vo;

import pers.mihao.ancient_empire.common.config.ErrorCodeHelper;
import pers.mihao.ancient_empire.common.constant.CommonConstant;

/**
 * 自定义错误类型
 *
 * @author mihao
 */
public class BaseException extends RuntimeException {

    private Integer code;
    private String mes;


    public BaseException(Exception e) {
        super(e);
        this.code = CommonConstant.ERROR;
    }

    public BaseException(Integer code) {
        super(ErrorCodeHelper.getErrorMes(code));
        this.code = code;
        this.mes = super.getMessage();
    }


    public BaseException(String mes) {
        super(mes);
        this.mes = mes;
        this.code = CommonConstant.ERROR;
    }

    public BaseException(Integer code, String mes) {
        this.code = code;
        this.mes = mes;
    }

    public BaseException() {
        this.code = CommonConstant.ERROR;
        this.mes = CommonConstant.DEFAULT_ERROR;
    }

    public Integer getCode() {
        return code;
    }

    public String getMes() {
        return mes;
    }
}
