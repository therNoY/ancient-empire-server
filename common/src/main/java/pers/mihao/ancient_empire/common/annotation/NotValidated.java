package pers.mihao.ancient_empire.common.annotation;

import java.lang.annotation.*;

/**
 * 不校验注解 默认校验
 */
@Target({ElementType.TYPE, ElementType.METHOD, ElementType.PARAMETER, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface NotValidated {
}
