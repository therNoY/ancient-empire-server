package pers.mihao.ancient_empire.base.reflact;

import org.hibernate.validator.constraints.Range;
import pers.mihao.ancient_empire.common.annotation.ValidatedBean;
import pers.mihao.ancient_empire.common.annotation.validated.InEnum;
import pers.mihao.ancient_empire.common.annotation.validated.Length;
import pers.mihao.ancient_empire.common.enums.LanguageEnum;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @version 1.0
 * @author mihao
 * @date 2021\1\17 0017 19:49
 */
@ValidatedBean
public class ValidateUtilTestBean {

    @NotNull
    private Integer id;

    @NotBlank(message = "非空")
    private String name;

    @Range(min = 10, max = 20)
    private Integer age;

    @InEnum(in = LanguageEnum.class)
    private String type;

    private ValidateUtilTestBean validateUtilTestBean1;

    private ValidateUtilTestBean validateUtilTestBean2;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public ValidateUtilTestBean getValidateUtilTestBean1() {
        return validateUtilTestBean1;
    }

    public void setValidateUtilTestBean1(ValidateUtilTestBean validateUtilTestBean1) {
        this.validateUtilTestBean1 = validateUtilTestBean1;
    }

    public ValidateUtilTestBean getValidateUtilTestBean2() {
        return validateUtilTestBean2;
    }

    public void setValidateUtilTestBean2(ValidateUtilTestBean validateUtilTestBean2) {
        this.validateUtilTestBean2 = validateUtilTestBean2;
    }
}
