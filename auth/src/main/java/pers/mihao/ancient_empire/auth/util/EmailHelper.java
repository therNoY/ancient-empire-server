package pers.mihao.ancient_empire.auth.util;

import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;
import pers.mihao.ancient_empire.common.util.ApplicationContextHolder;

public class EmailHelper {


    public static String getRegisterTemp(String url) {
        Context context = new Context();
        context.setVariable("url", url);
        ITemplateEngine templateEngine = ApplicationContextHolder.getBean(ITemplateEngine.class);
        String info = templateEngine.process("register.html", context);
        return info;
    }
}
