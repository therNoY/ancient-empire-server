package pers.mihao.ancient_empire.auth.annotation;

import pers.mihao.ancient_empire.auth.annotation.handle.SystemUserNameValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.NotBlank;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = SystemUserNameValidator.class)
public @interface SystemUserName {

	String message() default SystemUserNameValidator.ERROR_INFO;

	Class<?>[] groups() default { };

	Class<? extends Payload>[] payload() default { };
}
