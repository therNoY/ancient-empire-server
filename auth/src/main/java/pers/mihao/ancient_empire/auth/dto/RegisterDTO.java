package pers.mihao.ancient_empire.auth.dto;

import pers.mihao.ancient_empire.auth.annotation.SystemUserName;

import java.io.Serializable;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public class RegisterDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 手机号
	 */
	private String phone;
	/**
	 * 注册用户名
	 */
	@SystemUserName
	private String userName;
	/**
	 * 邮箱格式
	 */
	@Email(message = "邮箱格式不正确")
	@NotBlank(message = "邮箱不能为空")
	private String email;
	/**
	 * 密码
	 */
	@NotBlank(message = "密码不能为空")
	private String password;

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
