package pers.mihao.ancient_empire.auth.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import pers.mihao.ancient_empire.auth.annotation.SystemUserName;
import pers.mihao.ancient_empire.auth.dto.RegisterDTO;
import pers.mihao.ancient_empire.common.base_catch.CatchUtil;
import pers.mihao.ancient_empire.common.util.IntegerUtil;
import pers.mihao.ancient_empire.common.vo.test_dto.Dog;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@RestController
public class TestController {

    Logger log = LoggerFactory.getLogger(this.getClass());

    @GetMapping("/get/{id}")
    public void getTest(@PathVariable String id) {
        CatchUtil.set(id, new Dog(IntegerUtil.getRandomIn(10), id));
        log.info("get id: {}", id);
        CatchUtil.get(id);
        CatchUtil.getObject(id, Dog.class);
    }

    @GetMapping("/get2")
    public void getTest2(@RequestParam @NotBlank String id, @RequestParam @Email String email, @RequestParam @SystemUserName String userName) {
        log.info("get id: {}", id);
        CatchUtil.get(id);
        CatchUtil.getObject(id, Dog.class);
    }

    @GetMapping("/testDontReturn")
    public void testDontReturn(HttpServletRequest request, HttpServletResponse response) {

        new Thread(()->{

            for (int i = 0; i < 10; i++) {
                log.info("劫持request:{}{}", i, request.getParameterMap());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }).start();
        response.setStatus(200);
        return;
    }

    @PostMapping("/post")
    public void postTest(@RequestBody RegisterDTO dog) {
        log.info("post dog: {}", dog);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteTest(@PathVariable @NotBlank String id) {
        log.info("delete id: {}", id);
    }

    @PutMapping("/put")
    public void putTest(@RequestBody Dog dog) {
        log.info("put dog: {}", dog);
    }

    @RequestMapping("/root/get")
    public void getRootMes() {
        log.info("只有管理员才能看到");
    }

    @RequestMapping("/api/get")
    public void getApiMes() {
        log.info("只有登录过才能看到");
    }
}
