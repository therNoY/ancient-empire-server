package pers.mihao.ancient_empire.base.dto;

import pers.mihao.ancient_empire.common.dto.ApiRequestDTO;

/**
 * @Author mh32736
 * @Date 2021/7/28 20:59
 */
public class ReqRevertPaintingDTO extends ApiRequestDTO {

    private String revertType;


    public String getRevertType() {
        return revertType;
    }

    public void setRevertType(String revertType) {
        this.revertType = revertType;
    }
}
