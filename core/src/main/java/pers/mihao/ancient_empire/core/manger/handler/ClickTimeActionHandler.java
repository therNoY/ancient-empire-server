package pers.mihao.ancient_empire.core.manger.handler;

import java.util.List;
import pers.mihao.ancient_empire.base.bo.Site;
import pers.mihao.ancient_empire.core.constans.ExtMes;
import pers.mihao.ancient_empire.core.eums.GameCommendEnum;
import pers.mihao.ancient_empire.core.eums.StatusMachineEnum;
import pers.mihao.ancient_empire.core.manger.event.GameEvent;
import pers.mihao.ancient_empire.core.manger.strategy.attach.AttachStrategy;

/**
 * 支援事件 {@link pers.mihao.ancient_empire.core.eums.GameEventEnum.CLICK_TIME_ACTION}
 *
 * @Author mihao
 * @Date 2020/9/17 16:07
 */
public class ClickTimeActionHandler extends CommonHandler {

    @Override
    public void handlerCurrentUserGameEvent(GameEvent gameEvent) {
        List<Site> attachArea = AttachStrategy.getInstance()
            .getAttachArea(currUnit().getUnitMes(), currSite(), gameMap());
        commandStream()
            .toGameCommand().addCommand(GameCommendEnum.DIS_SHOW_ACTION)
            .toGameCommand().addCommand(GameCommendEnum.SHOW_ATTACH_AREA, ExtMes.ATTACH_AREA, attachArea);
        gameContext.setStatusMachine(StatusMachineEnum.WILL_TIME);
        gameContext.setWillAttachArea(attachArea);
    }
}
