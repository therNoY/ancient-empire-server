package pers.mihao.ancient_empire.core.manger.strategy.action;

import pers.mihao.ancient_empire.base.bo.Army;
import pers.mihao.ancient_empire.base.bo.BaseSquare;
import pers.mihao.ancient_empire.base.bo.Site;
import pers.mihao.ancient_empire.base.entity.UserRecord;
import pers.mihao.ancient_empire.base.enums.RegionEnum;
import pers.mihao.ancient_empire.base.util.AppUtil;
import pers.mihao.ancient_empire.common.util.StringUtil;
import pers.mihao.ancient_empire.core.eums.ActionEnum;

import java.util.ArrayList;
import java.util.List;


/**
 * @author mihao
 */
public class VillageGetActionStrategy extends ActionStrategy {

    /**
     * 获取 有占领村庄能力者 是否可以能占领
     *
     * @param sites   攻击范围
     * @param record
     * @param aimSite
     * @return
     */
    @Override
    public List<String> getAction(List<Site> sites, UserRecord record, Site aimSite) {
        List<String> actions = new ArrayList<>(1);
        // 获取要移动到的地址
        BaseSquare region = AppUtil
            .getRegionByPosition(record.getGameMap().getRegions(), aimSite.getRow(), aimSite.getColumn(),
                record.getGameMap().getColumn());
        // 判断是城镇
        if (region.getType().equals(RegionEnum.TOWN.type())) {
            // 判断不是友方城镇
            if (StringUtil.isBlack(region.getColor())) {
                actions.add(ActionEnum.OCCUPIED.type());
                return actions;
            }
            Army army = AppUtil.getArmyByColor(record, region.getColor());
            if (!army.getCamp().equals(record.getCurrCamp())) {
                actions.add(ActionEnum.OCCUPIED.type());
            }
        }
        return actions;
    }
}
