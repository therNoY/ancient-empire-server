package pers.mihao.ancient_empire.core.manger.strategy.action;

import java.util.ArrayList;
import java.util.List;
import pers.mihao.ancient_empire.base.bo.Army;
import pers.mihao.ancient_empire.base.bo.Site;
import pers.mihao.ancient_empire.base.bo.Unit;
import pers.mihao.ancient_empire.base.entity.Ability;
import pers.mihao.ancient_empire.base.entity.UserRecord;
import pers.mihao.ancient_empire.base.enums.AbilityEnum;
import pers.mihao.ancient_empire.core.eums.ActionEnum;

/**
 * @Author mh32736
 * @Date 2021/7/28 23:10
 */
public class SupporterActionStrategy extends ActionStrategy {

    @Override
    public List<String> getAction(List<Site> sites, UserRecord record, Site aimSite) {
        List<String> actions = new ArrayList<>(1);
        int camp = record.getArmyList().get(record.getCurrArmyIndex()).getCamp();
        List<Army> armies = record.getArmyList();
        List<Ability> abilities;
        for (Army army : armies) {
            if (!army.getCamp().equals(camp)) {
                continue;
            }
            for (Unit unit : army.getUnits()) {
                if (Boolean.TRUE.equals(unit.getDone()) && unit.getLevel() <= record.getCurrUnit().getLevel()) {
                    if ((abilities = abilityService.getUnitAbilityList(unit.getTypeId())) != null && !abilities
                        .contains(AbilityEnum.SUPPORTER.ability())) {
                        actions.add(ActionEnum.TIME.type());
                        return actions;
                    }
                }
            }
        }
        return actions;
    }
}
