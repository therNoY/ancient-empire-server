package pers.mihao.ancient_empire.core.manger.strategy.move_area;

import pers.mihao.ancient_empire.base.bo.GameMap;
import pers.mihao.ancient_empire.base.bo.Region;
import pers.mihao.ancient_empire.base.enums.RegionEnum;

import java.util.List;

/**
 * @author mihao
 */
public class WaterCloseMoveAreaStrategy extends MoveAreaStrategy {

    /**
     * 判断如果是水就返回 1
     *
     * @return
     */
    @Override
    protected int getRegionDeplete(GameMap gameMap, int index) {
        List<Region> regionList = gameMap.getRegions();
        String type = regionList.get(index).getType();
        if (type.startsWith(RegionEnum.SEA.type()) || type.startsWith(RegionEnum.BANK.type()) || type.startsWith(RegionEnum.BRIDGE.type())) {
            return 1;
        } else {
            return super.getRegionDeplete(gameMap, index);
        }
    }
}
