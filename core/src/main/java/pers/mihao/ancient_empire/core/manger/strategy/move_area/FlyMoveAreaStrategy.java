package pers.mihao.ancient_empire.core.manger.strategy.move_area;

import pers.mihao.ancient_empire.base.bo.GameMap;

/**
 * 飞行
 * @author mihao
 */
public class FlyMoveAreaStrategy extends MoveAreaStrategy {


    /**
     * 飞行对地形都是1
     * @return
     */
    @Override
    public int getRegionDeplete(GameMap gameMap, int index) {
        return 1;
    }
}
