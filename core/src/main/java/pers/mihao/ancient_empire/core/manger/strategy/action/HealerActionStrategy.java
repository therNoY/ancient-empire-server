package pers.mihao.ancient_empire.core.manger.strategy.action;

import java.util.ArrayList;
import java.util.List;
import pers.mihao.ancient_empire.base.bo.Army;
import pers.mihao.ancient_empire.base.bo.Site;
import pers.mihao.ancient_empire.base.bo.Unit;
import pers.mihao.ancient_empire.base.entity.UserRecord;
import pers.mihao.ancient_empire.base.enums.StateEnum;
import pers.mihao.ancient_empire.core.eums.ActionEnum;

/**
 * @Author mh32736
 * @Date 2021/7/28 21:32
 */
public class HealerActionStrategy extends ActionStrategy {

    @Override
    public List<String> getAction(List<Site> sites, UserRecord record, Site aimSite) {
        List<String> actions = new ArrayList<>(1);
        int camp = record.getArmyList().get(record.getCurrArmyIndex()).getCamp();
        List<Army> armies = record.getArmyList();
        for (Army army : armies) {
            if (army.getCamp().equals(camp)) {
                for (Unit unit : army.getUnits()) {
                    // 不中毒 再范围内 并且血量不全
                    if (!StateEnum.POISON.type().equals(unit.getStatus()) && unit.getLife() < unit.getMaxLife() && sites
                        .contains(unit)) {
                        actions.add(ActionEnum.HEAL.type());
                        return actions;
                    }
                }
            }
        }
        return actions;
    }
}
