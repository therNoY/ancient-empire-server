package pers.mihao.ancient_empire.core.manger.strategy.move_area;

import java.util.List;
import pers.mihao.ancient_empire.base.bo.GameMap;
import pers.mihao.ancient_empire.base.bo.Region;
import pers.mihao.ancient_empire.base.enums.RegionEnum;

/**
 * 大地之子
 * @Author mh32736
 * @Date 2021/7/29 10:57
 */
public class EarthCloseMoveAreaStrategy extends MoveAreaStrategy {

    /**
     * 大地之子对地形都是1
     *
     * @return
     */
    @Override
    public int getRegionDeplete(GameMap gameMap, int index) {
        List<Region> regionList = gameMap.getRegions();
        String type = regionList.get(index).getType();
        if (type.startsWith(RegionEnum.SEA.type()) || type.startsWith(RegionEnum.BANK.type()) || type
            .startsWith(RegionEnum.BRIDGE.type())) {
            return super.getRegionDeplete(gameMap, index) + 1;
        } else {
            return 1;
        }
    }
}
