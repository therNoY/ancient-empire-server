package pers.mihao.ancient_empire.core.eums;

import pers.mihao.ancient_empire.common.enums.BaseEnum;

/**
 * 攻击行为的枚举类
 * @author mihao
 */
public enum ActionEnum implements BaseEnum {
    /**
     * 攻击
     */
    ATTACK,
    /**
     * 购买
     */
    BUY,
    /**
     * 结束
     */
    END,
    /**
     * 移动
     */
    MOVE,
    /**
     * 占领
     */
    OCCUPIED,
    /**
     * 修复
     */
    REPAIR,
    /**
     * 召唤
     */
    SUMMON,
    /**
     * 治疗
     */
    HEAL,

    /**
     * 支援
     */
    TIME;
}
