#!bin/bash
# 初始化环境变量脚本
export workspace_path=/root/workspace
export runtime_path=/root/runtime
## 项目名称
export server_name=ancient-empire-server
export web_name=ancient-empire-app
## 日志路径
export runtime_log_path=${runtime_path}/log/ae.server.start.log
## 别名
export server_path=${workspace_path}/${server_name}
export runtim_server_path=${runtime_path}/${server_name}

export TOMCAT_PATH=/root/runtime/apache-tomcat-7.0.82/

export java_debug_port=8080

## 超时时间
export git_pull_time=30
export maven_build_time=30
export npm_install_time=30
export npm_build_time=30

## 是否忽略git拉娶代码报错true忽略
#export ignore_git_err=true


if [ -f $1/common_function.sh ]; then
  dos2unix $1/common_function.sh
  source $1/common_function.sh
else
  echo "文件common_function不存在"
  exit
fi
