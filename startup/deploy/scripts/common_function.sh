log() {
    start_time=$(date "+%Y-%m-%d %H:%M:%S")
    echo "["${start_time}"]["$2"]  "$1
    echo "["${start_time}"]["$2"]  "$1 >>${runtime_log_path} &2>>1
}

## info输出
infolog() {
    log $1 "info"
}

## 错误输出
errorlog() {
    log $1 "error"
}

## 执行命令直到执行成功
runcommand(){
  command=$1
  if [ ! $1 ]; then
      echo "没有要执行的命令"
      return
  fi

  if [ ! $2 ]; then
      check_command=$1
  else
      check_command=$2
  fi
  if [ ! $3 ]; then
      command_desc=执行命令:$1
  else
      command_desc=$3
  fi
  if [ ! $4 ]; then
      check_time=60
  else
      check_time=$4
  fi
  if [ ! $5 ]; then
      ignore_err=true
  else
      ignore_err=$5
  fi

  infolog ${command_desc}"开始执行 command: "${command}
  ${command} >>${runtime_log_path} &2>>1
  # 检测是否还在拉取代码中
  i=${check_time}
  while [[ $i -gt 0 ]];do
      sleep 2
      check_start=`ps -ef|grep ${check_command}|grep -v grep| awk '{printf $2}'`
      if [ -z "$check_start" ];
      then
          infolog "o(*￣▽￣*)ブ["${command_desc}"]执行成功"
          break
      fi
      infolog "第"$((${check_time}-${i}+1))"次检查,"${command_desc}"中..."
      ((i = i - 1))
  done
  if [ ${ignore_err} -eq "true" ];then
    infolog "忽略"${command_desc}"失败"
  elif [ $? -eq 0 ]; then
    errorlog ${command_desc}"退出执行(T_T)"
    exit
  fi
}