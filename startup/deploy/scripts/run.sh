# resolve links - $0 may be a softlink
PRG="$0"

while [ -h "$PRG" ] ; do
  ls=`ls -ld "$PRG"`
  link=`expr "$ls" : '.*-> \(.*\)$'`
  if expr "$link" : '/.*' > /dev/null; then
    PRG="$link"
  else
    PRG=`dirname "$PRG"`/"$link"
  fi
done

PRGDIR=`dirname "$PRG"`
shellpath=$(pwd)

if [ -f ${PRGDIR}/init_environment.sh ]; then
    echo "函数执行开始"
    dos2unix ${PRGDIR}/init_environment.sh
    source ${PRGDIR}/init_environment.sh ${PRGDIR}
    infolog "设置环境变量结束"

    infolog "执行后端构建脚本开始路径"$(pwd)"脚本路径:"${PRGDIR}/package_and_start.sh
    if [ -f ${PRGDIR}/package_and_start.sh ]; then
      dos2unix ${PRGDIR}/package_and_start.sh
      source ${PRGDIR}/package_and_start.sh ${PRGDIR}
    else
      errorlog "package_and_start脚本不存在"
    fi

    cd ${shellpath}
    infolog "执行前端构建脚本开始路径"$(pwd)"脚本路径:"${PRGDIR}/stratup_web.sh
    if [ -f ${PRGDIR}/stratup_web.sh ]; then
      dos2unix ${PRGDIR}/stratup_web.sh
      source ${PRGDIR}/stratup_web.sh ${PRGDIR}
    else
      errorlog "stratup_web脚本不存在"
    fi
else
    echo "配置文件init_environment不存在"
fi



