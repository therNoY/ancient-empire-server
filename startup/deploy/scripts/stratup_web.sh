#!bin/bash
echo "自动部署ae前端开始.........."
# 加载环境
source $1/init_environment.sh
# 获取当前文件夹
shelldir=$(pwd)/$1
infolog "执行更新和打包,当前文件夹"${currdir}
cd ${workspace_path}/${web_name}
runcommand "git pull" pull "更新前端代码" ${git_pull_time} ${ignore_git_err}
runcommand "npm install" "npm" "安装最新依赖" ${npm_install_time} ${ignore_git_err}
rm -rf ./dist
runcommand "npm run build" "npm" "前端打包h5" ${npm_install_time} ${ignore_git_err}

infolog "移动前端文件"$(pwd)
if [ -f ./dist/build/h5/index.html ]; then
    echo "停止tomcat服务"
    # ${TOMCAT_PATH}/bin/shutdown.sh
    rm -rf ${TOMCAT_PATH}/webapps/ROOT
    cp -r ./dist/build/h5 ${TOMCAT_PATH}/webapps/ROOT/
    echo "前端部署成功"
else
    echo "打包失败文件不存在 前端部署失败"
fi