#!bin/bash
# 加载环境
source $1/init_environment.sh
# 获取当前文件夹
shelldir=$(pwd)/$1
pwdpath=$(pwd)
infolog "执行更新和打包,当前文件夹"${shelldir}
cd ${server_path}
## 更新代码
runcommand "git pull" "pull" "更新后端代码" ${git_pull_time} ${ignore_git_err}
## 打包编译
runcommand "mvn clean package" "maven" "maven构建tar包" ${maven_build_time}

## 移动文件夹 调用
fileName=`ls ./startup/target | grep tar.gz`
if [ -f ./startup/target/${fileName} ]; then
    infolog "打包成功文件"${fileName}"开始移动文件"
    # 删除原来的文件
    rm -rf ${runtime_path}/${fileName} ${runtime_path}/${server_name}
    mv ./startup/target/${fileName} ${runtime_path}/
    sleep 2
    cd ${runtime_path}
    infolog "解压缩文件"${fileName}
    tar -xzf ./${fileName}
    sleep 5
    infolog "解压缩文件结束，准备更新脚本"
    mv ./${server_name}/scripts/* ${shelldir}
    infolog "准备启动后端项目,路径: "${shelldir}
    cd ${pwdpath}
    dos2unix $1/server_stratup.sh
    /bin/bash $1/server_stratup.sh $1
else
    errorlog "打包失败文件不存在"
fi

