#!bin/bash
echo "加载资源文件路径"$1/init_environment.sh
source $1/init_environment.sh

infolog "自动部署后端开始"

cd ${runtime_path}/${server_name}
if [ -f ./${server_name}.jar ]; then
    active_app=`ps -ef|grep ${server_name}.jar|grep -v grep|awk '{print $2}'`
    if [ "$active_app" ];
    then
        kill -9 ${active_app}
    fi
    nohup java -jar -Dfile.encoding=UTF-8 -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=${java_debug_port} ./${server_name}.jar jfile=config/application.properties >/dev/null &2>${runtime_log_path}
    infolog "自动部署成功"
else
    errorlog "打包失败文件不存在"
fi